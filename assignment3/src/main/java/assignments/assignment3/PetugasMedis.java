package assignments.assignment3;

public class PetugasMedis extends Manusia{
  		
    private int jumlahDisembuhkan;

    public PetugasMedis(String name){
        // TODO: Buat constructor untuk Jurnalis.
        // Hint: Akses constructor superclass-nya
    	super(name);
    }

    public void obati(Manusia manusia) {
        // TODO: Implementasikan apabila objek PetugasMedis ini menyembuhkan manusia
        // Hint: Update nilai atribut jumlahDisembuhkan
    	manusia.ubahStatus("negatif");
    	manusia.getRantaiPenular().clear();
    	
    	for (int i = 0; i < manusia.getRantaiPenular().size(); i++) {
    		boolean foundDuplicate = false;
    		Carrier c = manusia.getRantaiPenular().get(i);
    		
    		for (int j = i + 1; j < manusia.getRantaiPenular().size(); j++) {
    			Carrier c2 = manusia.getRantaiPenular().get(j);
    			if (c == c2) {
    				foundDuplicate = true;
    			}
    		}
    		
    		if (!foundDuplicate) {
    			c.subAktifKasusDisebabkan();
    		}
    	}
    	
    	tambahSembuh();
    	this.jumlahDisembuhkan++;
    }

    public int getJumlahDisembuhkan(){
        // TODO: Kembalikan nilai dari atribut jumlahDisembuhkan
        return jumlahDisembuhkan;
    }
    
    public String toString() {
    	return "PETUGAS MEDIS";
    }
}