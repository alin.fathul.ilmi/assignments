package assignments.assignment3;

public class Positif implements Status{
  
    public String getStatus(){
        return "Positif";
    }

    public void tularkan(Carrier penular, Carrier tertular){
        // TODO: Implementasikan apabila object Penular melakukan interaksi dengan object tertular
        // Hint: Handle kasus ketika keduanya benda dapat dilakukan disini
    	if (penular.getStatusCovid().equals("Positif") && tertular.getStatusCovid().equals("Positif")) {
    		return;
    	}
    	
    	for (int i = 0; i < penular.getRantaiPenular().size(); i++) {
    		boolean foundDuplicate = false;
    		Carrier c = penular.getRantaiPenular().get(i);
    		
    		for (int j = i + 1; j < penular.getRantaiPenular().size(); j++) {
    			Carrier c2 = penular.getRantaiPenular().get(j);
    			if (c == c2) {
    				foundDuplicate = true;
    			}
    		}
    		
    		if (!foundDuplicate && c.getStatusCovid().equals("Positif")) {
    			c.addAktifKasusDisebabkan();
    			c.addTotalKasusDisebabkan();
    		}
    	}
    	
    	penular.addTotalKasusDisebabkan();
    	penular.addAktifKasusDisebabkan();
    	
    	tertular.getRantaiPenular().addAll(penular.getRantaiPenular());
    	tertular.getRantaiPenular().add(penular);
    	tertular.ubahStatus("positif");
    }
}