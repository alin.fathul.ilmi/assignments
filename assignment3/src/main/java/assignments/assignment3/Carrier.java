package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public abstract class Carrier{

    private String nama;
    private String tipe;
    private Status statusCovid;
    private int aktifKasusDisebabkan;
    private int totalKasusDisebabkan;
    private List<Carrier> rantaiPenular;

    public Carrier(String nama, String tipe){
        // TODO: Buat constructor untuk Carrier.
    	this.nama = nama;
    	this.tipe = tipe;
    	this.statusCovid = new Negatif();
    	this.rantaiPenular = new ArrayList<Carrier>();
    }

    public String getNama(){
        // TODO : Kembalikan nilai dari atribut nama
        return this.nama;
    }

    public String getTipe(){
        // TODO : Kembalikan nilai dari atribut tipe
        return this.tipe;
    }

    //METHOD TAMBAHAN AGAR BISA MENGKECALL TULARKAN DARI CLASS BENDA DAN MANUSIA
    public Status getStatus() {
    	return this.statusCovid;
    }
    
    public String getStatusCovid(){
        // TODO : Kembalikan nilai dari atribut statusCovid
        return this.statusCovid.getStatus();
    }

    public int getAktifKasusDisebabkan(){
        // TODO : Kembalikan nilai dari atribut aktifKasusDisebabkan
        return this.aktifKasusDisebabkan;
    }

    public int getTotalKasusDisebabkan(){
        // TODO : Kembalikan nilai dari atribut totalKasusDisebabkan
        return this.totalKasusDisebabkan;
    }
    
    public void addAktifKasusDisebabkan() {
    	this.aktifKasusDisebabkan++;
    }
    
    public void subAktifKasusDisebabkan() {
    	this.aktifKasusDisebabkan--;
    }
    
    public void addTotalKasusDisebabkan() {
    	this.totalKasusDisebabkan++;
    }

    public List<Carrier> getRantaiPenular(){
        // TODO : Kembalikan nilai dari atribut rantaiPenular
        return rantaiPenular;
    }

    public void ubahStatus(String status){
        // TODO : Implementasikan fungsi ini untuk mengubah atribut dari statusCovid
    	if (status == "positif") {
    		this.statusCovid = new Positif();
    	}
    	else {
    		this.statusCovid = new Negatif();
    	}
    }

    public void interaksi(Carrier lain){
        // TODO : Objek ini berinteraksi dengan objek lain

    }

    public abstract String toString();

}
