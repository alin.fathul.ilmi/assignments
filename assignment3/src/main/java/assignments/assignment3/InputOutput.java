package assignments.assignment3;

import java.io.*;

public class InputOutput{
  	
    private BufferedReader br;
    private PrintWriter pw;
    private String inputFile;
    private String outputFile; 
    private World world;

    public InputOutput(String inputType, String inputFile, String outputType, String outputFile){
        // TODO: Buat constructor untuk InputOutput.
    	this.inputFile = inputFile;
    	this.outputFile = outputFile;
    	this.world = new World();
    	
    	try {
    		setBufferedReader(inputType);
			setPrintWriter(outputType);
		} catch (IOException e) {
			pw.println("FILE TIDAK ADA");
			pw.flush();
		}
    }

    public void setBufferedReader(String inputType) throws IOException{
        // TODO: Membuat BufferedReader bergantung inputType (I/O text atau input terminal) 
    	if (inputType.equals("TEXT")) {
			this.br = new BufferedReader(new FileReader(this.inputFile));
    	}
    	else {
    		this.br = new BufferedReader(new InputStreamReader(System.in));	
    	}
    }
    
    public void setPrintWriter(String outputType) throws IOException{
        // TODO: Membuat PrintWriter bergantung inputType (I/O text atau output terminal) 
    	if (outputType.equals("TEXT")) {
        	this.pw = new PrintWriter(new File(this.outputFile));
    	}
    	else {
    		this.pw = new PrintWriter(System.out, true);
    	}
    }
    
    public void run() throws IOException{
        // TODO: Program utama untuk InputOutput, jangan lupa handle untuk IOException
        // Hint: Buatlah objek dengan class World
        // Hint: Untuk membuat object Carrier baru dapat gunakan method CreateObject pada class World
        // Hint: Untuk mengambil object Carrier dengan nama yang sesua dapat gunakan method getCarrier pada class World
    	String command = br.readLine();
    	String[] splitCmd = command.split(" ");

    	while (!splitCmd[0].toUpperCase().equals("EXIT")) {

    		switch (splitCmd[0].toUpperCase()) {
    		case "ADD":
    			if (splitCmd.length != 3) { break; }
    			add(splitCmd[1], splitCmd[2]);
    			break;
    		case "INTERAKSI":
    			if (splitCmd.length != 3) { break; }
    			interaksi(this.world.getCarrier(splitCmd[1]), this.world.getCarrier(splitCmd[2]));
    			break;
    		case "POSITIFKAN":
    			if (splitCmd.length != 2) { break; }
    			positifkan(this.world.getCarrier(splitCmd[1]));
    			break;
    		case "SEMBUHKAN":
    			if (splitCmd.length != 3) { break; }
    			sembuhkan((PetugasMedis)this.world.getCarrier(splitCmd[1]), (Manusia)this.world.getCarrier(splitCmd[2]));
    			break;
    		case "BERSIHKAN":
    			if (splitCmd.length != 3) { break; }
    			bersihkan((CleaningService)this.world.getCarrier(splitCmd[1]), (Benda)this.world.getCarrier(splitCmd[2]));
    			break;
    		case "RANTAI":
    			if (splitCmd.length != 2) { break; }
    			try {
					rantai(this.world.getCarrier(splitCmd[1]));
				} catch (BelumTertularException e) {
					pw.println(e);
					pw.flush();
				}
    			break;
    		case "TOTAL_KASUS_DARI_OBJEK":
    			if (splitCmd.length != 2) { break; }
    			totalKasusDariObjek(this.world.getCarrier(splitCmd[1]));
    			break;
    		case "AKTIF_KASUS_DARI_OBJEK":
    			if (splitCmd.length != 2) { break; }
    			aktifKasusDariObjek(this.world.getCarrier(splitCmd[1]));
    			break;
    		case "TOTAL_SEMBUH_MANUSIA":
    			if (splitCmd.length != 1) { break; }
    			totalSembuhManusia();
    			break;
    		case "TOTAL_SEMBUH_PETUGAS_MEDIS":
    			if (splitCmd.length != 2) { break; }
    			totalSembuhPetugasMedis((PetugasMedis)this.world.getCarrier(splitCmd[1]));
    			break;
    		case "TOTAL_BERSIH_CLEANING_SERVICE":
    			if (splitCmd.length != 2) { break; }
    			totalBersihCleaningService((CleaningService)this.world.getCarrier(splitCmd[1]));
    			break;
    		case "GETSTATUS":
    			for (Carrier cr : this.world.listcar()) {
    				pw.println(cr.getStatusCovid() + " " + cr.getNama());
    			}
    			break;
    		case "GETPERSEN":
    			for (Carrier cr : this.world.listcar()){
    				if (cr.getTipe().equals("Benda")) {
    					Benda b = (Benda)cr;
    					pw.println(b.getPersentaseMenular() + ", " + b.getNama());
    				}
    			}
    			break;
    		default:
    			pw.println("COMMAND TIDAK ADA");
    			pw.flush();
    			break;
    		}
    		
    		command = br.readLine();
    		splitCmd = command.split(" ");
    	}
    	
    	br.close();
    	pw.close();
    }
    
    private void add(String tipe, String nama) {
    	this.world.createObject(tipe, nama);
    }
    
    private void interaksi(Carrier carrier1, Carrier carrier2) {
    	carrier1.interaksi(carrier2);
    	carrier2.interaksi(carrier1);
    }
    
    private void positifkan(Carrier carrier) {
    	carrier.ubahStatus("positif");
    }
    
    private void sembuhkan(PetugasMedis petugasMedis, Manusia manusia) {
    	petugasMedis.obati(manusia);
    }
    
    private void bersihkan(CleaningService cleaningService, Benda benda) {
    	cleaningService.bersihkan(benda);
    }
    
    private void rantai(Carrier carrier) throws BelumTertularException {
    	if (carrier.getStatusCovid().equals("Negatif")) {
    		throw new BelumTertularException(carrier.toString() + " " + carrier.getNama()
    										+ " Berstatus negatif");
    	}
    	
    	pw.print("Rantai Penyebaran " + carrier.toString() + " " + carrier.getNama() + ": ");
    	
    	for (Carrier c : carrier.getRantaiPenular()) {
    		pw.print(c.toString() + " " + c.getNama() + "-> ");        	
    	}
    	
    	pw.print(carrier.toString() + " " + carrier.getNama());
    	
    	pw.println();
    	
    	pw.flush();
    }
    
    private void totalKasusDariObjek(Carrier carrier) {
    	pw.println(carrier.toString() + " " + carrier.getNama() + " telah menyebarkan virus "
    			+ "COVID ke " + carrier.getTotalKasusDisebabkan() + " objek");
    	pw.flush();
    }
    
    private void aktifKasusDariObjek(Carrier carrier) {
    	pw.println(carrier.toString() + " " + carrier.getNama() + " telah menyebarkan virus "
    			+ "COVID dan masih teridentifikasikan positif sebanyak " + carrier.getAktifKasusDisebabkan() + " objek");
    	pw.flush();
    }
    
    private void totalSembuhManusia() {
    	pw.println("Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: " + Manusia.getJumlahSembuh() + " kasus");
    	pw.flush();
    }
    
    private void totalSembuhPetugasMedis(PetugasMedis petugasMedis) {
    	pw.println(petugasMedis.toString() + " " + petugasMedis.getNama() + " menyembuhkan " +
                 petugasMedis.getJumlahDisembuhkan() + " manusia");
    	pw.flush();
    }
    
    private void totalBersihCleaningService(CleaningService cleaningService) {
    	pw.print(cleaningService.toString() + " " + cleaningService.getNama() + "Membersihkan " 
    			+ cleaningService.getJumlahDibersihkan() + " Benda");
    	pw.flush();
    }
}