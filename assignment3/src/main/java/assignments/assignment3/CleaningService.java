package assignments.assignment3;

public class CleaningService extends Manusia{
  		
    private int jumlahDibersihkan;

    public CleaningService(String name){
        // TODO: Buat constructor untuk CleaningService.
        // Hint: Akses constructor superclass-nya
    	super(name);
    }

    public void bersihkan(Benda benda){
        // TODO: Implementasikan apabila objek CleaningService ini membersihkan benda
        // Hint: Update nilai atribut jumlahDibersihkan
    	if (benda.getPersentaseMenular() >= 0) {
    		benda.setPersentaseMenular(0);
    		benda.ubahStatus("negatif");
    	}
    	
    	this.jumlahDibersihkan++;
    }

    public int getJumlahDibersihkan(){
        // TODO: Kembalikan nilai dari atribut jumlahDibersihkan
        return jumlahDibersihkan;
    }
    
    public String toString() {
    	return "CLEANING SERVICE";
    }

}