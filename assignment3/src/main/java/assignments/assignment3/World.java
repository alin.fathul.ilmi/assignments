package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class World{

    public List<Carrier> listCarrier;

    public World(){
        // TODO: Buat constructor untuk class World
    	this.listCarrier = new ArrayList<Carrier>();
    }

    public Carrier createObject(String tipe, String nama){
        // TODO: Implementasikan apabila ingin membuat object sesuai dengan parameter yang diberikan
    	switch (tipe.toUpperCase()) {
    	case "CLEANING_SERVICE":
    		CleaningService cleaningService = new CleaningService(nama);
    		this.listCarrier.add(cleaningService);
    		return cleaningService;
    	case "JURNALIS":
    		Jurnalis jurnalis = new Jurnalis(nama);
    		this.listCarrier.add(jurnalis);
    		return jurnalis;
    	case "OJOL":
    		Ojol ojol = new Ojol(nama);
    		this.listCarrier.add(ojol);
    		return ojol;
    	case "PEKERJA_JASA":
    		PekerjaJasa pekerjaJasa = new PekerjaJasa(nama);
    		this.listCarrier.add(pekerjaJasa);
    		return pekerjaJasa;
    	case "PETUGAS_MEDIS":
    		PetugasMedis petugasMedis = new PetugasMedis(nama);
    		this.listCarrier.add(petugasMedis);
    		return petugasMedis;
    	case "ANGKUTAN_UMUM":
    		AngkutanUmum angkutanUmum = new AngkutanUmum(nama);
    		this.listCarrier.add(angkutanUmum);
    		return angkutanUmum;
    	case "PEGANGAN_TANGGA":
    		PeganganTangga peganganTangga = new PeganganTangga(nama);
    		this.listCarrier.add(peganganTangga);
    		return peganganTangga;
    	case "PINTU":
    		Pintu pintu = new Pintu(nama);
    		this.listCarrier.add(pintu);
    		return pintu;
    	case "TOMBOL_LIFT":
    		TombolLift tombolLift = new TombolLift(nama);
    		this.listCarrier.add(tombolLift);
    		return tombolLift;
    	default:
    		return null;
    	}
    }

    public List<Carrier> listcar() {
    	return listCarrier;
    }
    
    
    public Carrier getCarrier(String nama){
        // TODO: Implementasikan apabila ingin mengambil object carrier dengan nama sesuai dengan parameter
        for (Carrier carrier : this.listCarrier) {
        	if (carrier.getNama().equals(nama)) {
        		return carrier;
        	}
        }
    	
    	return null;
    }
}
