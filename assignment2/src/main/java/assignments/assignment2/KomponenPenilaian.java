package assignments.assignment2;

public class KomponenPenilaian {
    private String nama;
    private ButirPenilaian[] butirPenilaian;
    private int bobot;

    public KomponenPenilaian(String nama, int banyakButirPenilaian, int bobot) {
        // Note: banyakButirPenilaian digunakan untuk menentukan panjang butirPenilaian saja
        // (tanpa membuat objek-objek ButirPenilaian-nya).
    	this.nama = nama;
    	this.butirPenilaian = new ButirPenilaian[banyakButirPenilaian];
    	this.bobot = bobot;
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian templat.
     * @param templat templat KomponenPenilaian.
     */
    private KomponenPenilaian(KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat) {
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    public void masukkanButirPenilaian(int idx, ButirPenilaian butir) {
    	if ((idx < 0) || (idx >= this.butirPenilaian.length)) { return; }
    	butirPenilaian[idx] = butir;
    }

    public String getNama() {
        return nama;
    }

    public double getRerata() {
    	double jmlNilai = 0;
    	int jmlIndex = 0;
    	
    	for (ButirPenilaian butir : this.butirPenilaian)
    	{
    		if (butir != null)
    		{
    			jmlIndex++;
    			jmlNilai += butir.getNilai();
    		}
    	}
    	
    	if (jmlIndex == 0) { return 0.0; }
        return jmlNilai / jmlIndex;
    }

    public double getNilai() {
        return getRerata() * bobot / 100;
    }

    public String getDetail() {
    	String semuaNilai = "";
    	int index = 0;
    	
    	for (ButirPenilaian butir : this.butirPenilaian)
    	{
    		if (butir != null)
    		{
    			semuaNilai += String.format("%s %d: %s%n", this.nama, ++index, butir);
    		}
    	}
    	
    	return String.format("~~~ %s (%d%%) ~~~%n"
    			           + "%s"
    			           + "%s"
    			           + "Kontribusi Nilai Akhir: %.2f%n", this.nama, this.bobot, semuaNilai, (index > 1) ? String.format("Rerata : %.2f%n", getRerata()) : "", getNilai());
    }

    @Override
    public String toString() {
        return String.format("Rerata %s: %.2f", this.nama, getRerata());
    }

}
