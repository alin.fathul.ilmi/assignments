package assignments.assignment2;

public class Mahasiswa implements Comparable<Mahasiswa> {
    private String npm;
    private String nama;
    private KomponenPenilaian[] komponenPenilaian;

    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian) {
        // Note: komponenPenilaian merupakan skema penilaian yang didapat dari GlasDOS.
    	this.npm = npm;
    	this.nama = nama;
    	this.komponenPenilaian = komponenPenilaian;
    }

    public KomponenPenilaian getKomponenPenilaian(String namaKomponen) {
        // Note: jika tidak ada, kembalikan null atau lempar sebuah Exception.
    	for (KomponenPenilaian komponen : this.komponenPenilaian)
    	{
    		if (komponen.getNama().equals(namaKomponen))
    		{
    			return komponen;
    		}
    	}
    	
        return null;
    }

    public String getNpm() {
        return this.npm;
    }

    /**
     * Mengembalikan huruf berdasarkan nilai yang diberikan.
     * @param nilaiAkhir nilai untuk dicari hurufnya.
     * @return huruf dari nilai.
     */
    private static String getHuruf(double nilai) {
        return nilai >= 85 ? "A" :
            nilai >= 80 ? "A-" :
            nilai >= 75 ? "B+" :
            nilai >= 70 ? "B" :
            nilai >= 65 ? "B-" :
            nilai >= 60 ? "C+" :
            nilai >= 55 ? "C" :
            nilai >= 40 ? "D" : "E";
    }

    /**
     * Mengembalikan status kelulusan berdasarkan nilaiAkhir yang diberikan.
     * @param nilaiAkhir nilai akhir mahasiswa.
     * @return status kelulusan (LULUS/TIDAK LULUS).
     */
    private static String getKelulusan(double nilaiAkhir) {
        return nilaiAkhir >= 55 ? "LULUS" : "TIDAK LULUS";
    }
    
    private String getHasil()
    {
    	//mengembalikan nilai akhir, huruf, dan lulus atau tidak
    	double nilaiAkhir = 0;
    	
    	for (KomponenPenilaian komponen : this.komponenPenilaian)
    	{
    		nilaiAkhir += komponen.getNilai();
    	}
    	
    	return String.format("Nilai Akhir: %.2f%n"
    			           + "Huruf: %s%n"
    			           + "%s%n", nilaiAkhir, getHuruf(nilaiAkhir), getKelulusan(nilaiAkhir));
    }

    public String rekap() {
    	String strRekap = "";
    	
    	for (KomponenPenilaian komponen : this.komponenPenilaian)
    	{
    		strRekap += String.format("%s%n", komponen);
    	}
    	
    	strRekap += getHasil();

        return strRekap;
    }

    public String toString() {
        return String.format("%s - %s", this.npm, this.nama);
    }

    public String getDetail() {
    	String strDetail = "";
    	
    	for (KomponenPenilaian komponen : this.komponenPenilaian)
    	{
    		strDetail += String.format("%s%n", komponen.getDetail());
    	}
    	
    	strDetail += getHasil();
    	
        return strDetail;
    }

    @Override
    public int compareTo(Mahasiswa other) {
        // Hint: bandingkan NPM-nya, String juga punya method compareTo.
        return this.npm.compareTo(other.getNpm());
    }
}
