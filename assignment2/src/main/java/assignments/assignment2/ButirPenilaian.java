package assignments.assignment2;

public class ButirPenilaian {
    private static final int PENALTI_KETERLAMBATAN = 20;
    private double nilai;
    private boolean terlambat;

    public ButirPenilaian(double nilai, boolean terlambat) {
    	this.nilai = nilai;
    	this.terlambat = terlambat;
    	
    	if (terlambat) { this.nilai -= nilai * 0.2; }
    }

    public double getNilai() {
    	if (nilai < 0) { return 0; }
    	
    	return nilai;
    }

    @Override
    public String toString() {
    	return (terlambat) ?  String.format("%.2f (T)", nilai) : String.format("%.2f", nilai);
    }
}
