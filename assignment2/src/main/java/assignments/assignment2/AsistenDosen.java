package assignments.assignment2;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class AsistenDosen {
    private List<Mahasiswa> mahasiswa = new ArrayList<>();
    private String kode;
    private String nama;

    public AsistenDosen(String kode, String nama) {
    	this.kode = kode;
    	this.nama = nama;
    }

    public String getKode() {
        return this.kode;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        // Hint: kamu boleh menggunakan Collections.sort atau melakukan sorting manual.
        // Note: manfaatkan method compareTo pada Mahasiswa.
    	this.mahasiswa.add(mahasiswa);
    	Collections.sort(this.mahasiswa);
    }

    public Mahasiswa getMahasiswa(String npm) {
        // Note: jika tidak ada, kembalikan null atau lempar sebuah Exception.
    	for (Mahasiswa mhs : this.mahasiswa)
    	{
    		if (mhs.getNpm().equals(npm))
    		{
    			return mhs;
    		}
    	}
    	
        return null;
    }

    public String rekap() {
    	String strRekap = "";
    	
    	for (Mahasiswa mhs : this.mahasiswa)
    	{
    		strRekap += String.format("%s%n%s%n", mhs, mhs.rekap());
    	}
    	
        return strRekap;
    }

    public String toString() {
        return String.format("%s - %s", this.kode, this.nama);
    }
}
